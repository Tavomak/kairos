function servicioBtn(){
    $("#servicio-form-submit").prop("disabled", false);
    $("#servicio-form-submit").removeClass("disabled");
}

    $("#servicioForm").validator().submit( function (e) {
    if (e.isDefaultPrevented()) {
        // handle the invalid form...
        servicioError();
        servicioSubmitMSG(false, "Mensaje invalido");
    } else {
        // everything looks good!
        e.preventDefault();
        servicioSubmitForm();
    }
});

function servicioSubmitForm(){



    var servicioData ={
        'nombre' : $('input[name=nombre]').val(),
        'telefono' : $('input[name=telefono]').val(),
        'correo' : $('input[name=correo]').val(),
        'servicio' : $("input[name='myChbx[]']:checked").val(),
        'mensaje' : $('#mensaje').val()

    }

    $.ajax({
        type: "POST",
        url: "servicio.php",
        data: servicioData,
        success : function(text){
            if (text == "Mensaje enviado :)"){
                servicioSuccess();
            } else {
                servicioError();
                servicioSubmitMSG(false,text);
            }
        }
    });
}

    function servicioSuccess(){
    $("#servicioForm")[0].reset();
    servicioSubmitMSG(true, "Mensaje enviado")
}

function servicioError(){
    $("#servicioForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
    $("#servicioForm")[0].reset();
}

function servicioSubmitMSG(valid, msg){
    if(valid){
        var msgClasses = "bounceInLeft animated text-success";
    } else {
        var msgClasses = "shake animated text-danger";
    }
    $("#servicioSubmit").removeClass().addClass(msgClasses).text(msg);
}
