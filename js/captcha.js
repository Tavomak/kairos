var recaptcha1;
var recaptcha2;
var onloadCallback = function() {
    //Render the recaptcha1 on the element with ID "recaptcha1"
    recaptcha1 = grecaptcha.render('recaptcha1', {
        'sitekey': '6LfWwxMUAAAAAJISrPtZTlFiv13P4flGx0oLW7m1'
        , 'theme': 'light'
        , 'callback': 'pagoBtn'
    });
    //Render the recaptcha2 on the element with ID "recaptcha2"
    recaptcha2 = grecaptcha.render('recaptcha2', {
        'sitekey': '6LfWwxMUAAAAAJISrPtZTlFiv13P4flGx0oLW7m1'
        , 'theme': 'light'
        , 'callback': 'servicioBtn'
    });
};
