function pagoBtn(){
    $("#pago-form-submit").prop("disabled", false);
    $("#pago-form-submit").removeClass("disabled");
}

$("#pagoForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        pagoError();
        pagoSubmitMSG(false, "Mensaje invalido");
    } else {
        // everything looks good!
        event.preventDefault();
        pagoSubmitForm();
    }
});

function pagoSubmitForm(){
    // Initiate Variables With Form Content
    var pagoData ={
        'banco' : $('input[name=banco]').val(),
        'transferencia' : $('input[name=transferencia]').val(),
        'operacion' : $('input[name=operacion]').val(),
        'monto' : $('input[name=monto]').val(),
        'titular' : $('input[name=titular]').val(),
        'clienteC' : $('input[name=clienteC]').val(),
        'clienteN' : $('input[name=clienteN]').val(),
        'email' : $('input[name=email]').val()

    }

    $.ajax({
        type: "POST",
        url: "pago.php",
        data: pagoData,
        success : function(text){
            if (text == "Mensaje enviado :)"){
                pagoSuccess();
            } else {
                pagoError();
                pagoSubmitMSG(false,text);
            }
        }
    });
}

function pagoSuccess(){
    $("#pagoForm")[0].reset();
    pagoSubmitMSG(true, "Mensaje enviado")
}

function pagoError(){
    $("#pagoForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
    $("#pagoForm")[0].reset();
}

function pagoSubmitMSG(valid, msg){
    if(valid){
        var msgClasses = "bounceInLeft animated text-success";
    } else {
        var msgClasses = "shake animated text-danger";
    }
    $("#pagoSubmit").removeClass().addClass(msgClasses).text(msg);
}
