<?php

$errorMSG = "";
//Correo de destino; donde se enviará el correo.
$correoDestino = "gps.kairos@gmail.com";

$banco = $_POST["banco"];
$transferencia = $_POST["transferencia"];
$operacion = $_POST["operacion"];
$monto = $_POST["monto"];
$titular = $_POST["titular"];
$clienteC = $_POST["clienteC"];
$clienteN = $_POST["clienteN"];
$email = $_POST["email"];

//Formateo el asunto del correo
$asunto = "Registro de pago WEB KAIROS por BsF. $monto";

//Texto emisor; sólo lo leerá quien reciba el contenido.
$textoEmisor = "MIME-VERSION: 1.0\r\n";
$textoEmisor .= "Content-type: text/html; charset=UTF-8\r\n";
$textoEmisor .= "From: kairosgps.com.ve";

//Formateo el cuerpo del correo

$cuerpo = "<h2>Nombre del cliente:</h2> " . "<h3>$clienteN</h3>" . "</hr>";
$cuerpo .= "<h2>Correo eléctronico:</h2> " . "<h3>$email</h3>" . "</hr>";
$cuerpo .= "<h2>CI del cliente:</h2> " . "<h3>$clienteC</h3>" . "</hr>";
$cuerpo .= "<h2>CI del Titular de la cuenta:</h2> " . "<h3>$titular</h3>" . "</hr>";
$cuerpo .= "<h2>Monto depositado:</h2> " . "<h3>$monto</h3>" . "</hr>";
$cuerpo .= "<h2>Fecha de operacion:</h2> " . "<h3>$operacion</h3>" . "</hr>";
$cuerpo .= "<h2>Banco:</h2> " . "<h3>$banco</h3>" . "</hr>";
$cuerpo .= "<h2>Numero de transferencia o deposito:</h2> " . "<h3>$transferencia</h3>";

// Envío el mensaje
$success = mail( $correoDestino, $asunto, $cuerpo, $textoEmisor);

// redirect to success page
if ($success && $errorMSG == ""){
   echo "Mensaje enviado :)";
}else{
    if($errorMSG == ""){
        echo "Algo va mal :(";
    } else {
        echo $errorMSG;
    }
}

?>
