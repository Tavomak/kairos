<?php

$errorMSG = "";
//Correo de destino; donde se enviará el correo.
$correoDestino = "gps.kairos@gmail.com";

$nombre = $_POST["nombre"];
$telefono = $_POST["telefono"];
$correo = $_POST["correo"];
$servicio = $_POST["servicio"];
$mensaje = $_POST["mensaje"];

//Formateo el asunto del correo
$asunto = "Solicitud de servicio WEB KAIROS $nombre";

//Texto emisor; sólo lo leerá quien reciba el contenido.
$textoEmisor = "MIME-VERSION: 1.0\r\n";
$textoEmisor .= "Content-type: text/html; charset=UTF-8\r\n";
$textoEmisor .= "From: kairosgps.com.ve";

$cuerpo = "<h2>Nombre del solicitante:</h2> " . "<h3>$nombre</h3>" ."<br />";
$cuerpo .= "<h2>Telefono:</h2> " . "<h3>$telefono</h3>" ."<br />";
$cuerpo .= "<h2>Correo eléctronico:</h2> " . "<h3>$correo</h3>" . "<br />";
$cuerpo .= "<h2>Servicio solicitado:</h2> " . "<h3>$servicio</h3>" . "<br />";
$cuerpo .= "<h2>Mensaje:</h2> " . "<h3>$mensaje</h3>" . "<br />";

// Envío el mensaje
$success = mail( $correoDestino, $asunto, $cuerpo, $textoEmisor);

// redirect to success page
if ($success && $errorMSG == ""){
   echo "Mensaje enviado :)";
}else{
    if($errorMSG == ""){
        echo "Algo va mal :(";
    } else {
        echo $errorMSG;
    }
}

?>
